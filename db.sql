CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `facebook_user_id` bigint(11) NOT NULL,
  `token_id` varchar(255) NOT NULL,
  `facebook_tooken_id` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`),
  UNIQUE KEY `fb_user_id` (`facebook_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;