from flask import Blueprint, jsonify, g

rest_api_v1 = Blueprint('rest_api_v1', __name__)

@rest_api_v1.route('/')
def show():
    res = g.db.execute('select * from user LIMIT 1').fetchall()
    return jsonify(key="Hello World!" + res[0].token_id)