from flask import Flask, jsonify, request, g, abort
from sqlalchemy import create_engine, text
from rest_api_v1 import rest_api_v1
import facebook, hashlib, time, random, socket, uuid

app = Flask(__name__)
app.register_blueprint(rest_api_v1, url_prefix='/v1')


def init_env():
    g.db = create_engine('mysql://bektok2:bektok@localhost/bektok2', convert_unicode=True).connect()
    g.auth = False
    g.salt = 'mx12970y87bc0_936vr-971cr7x8b6trvxb'


@app.before_request
def before_request():
    init_env()
    api_key = request.values.get('api_key', '')
    res = g.db.execute(text('select * from user where token_id = :key LIMIT 1'), {'key': api_key}).fetchall()
    if request.endpoint != 'fb_auth':
        if not (len(res) == 1 and res[0].token_id == api_key):
            abort(403)


@app.after_request
def before_request(response):
    g.db.close()
    return response


@app.errorhandler(403)
def forbidden(e):
    return 'WRONG APP API KEY', 403


@app.route("/fb_auth", methods=['GET'])
def fb_auth():
    fb_token = request.values.get('fb_api_key', None)
    if fb_token is not None:
        graph = facebook.GraphAPI(fb_token)
        profile = graph.get_object("me")
        if (profile['id'] is not None):
            res = g.db.execute(text('select * from user where facebook_user_id = :fb_uid LIMIT 1'), {'fb_uid': profile['id']}).fetchall()
            if len(res) == 1:
                #update FB token
                g.db.execute(text('UPDATE user SET facebook_tooken_id = :fb_token_id WHERE facebook_user_id = :fb_uid'), {'fb_uid': profile['id'], 'fb_token_id': fb_token})
                return res[0].token_id
            else:
                #create new application token
                guid_val = guid(g.salt,profile['id'])
                g.db.execute(text('INSERT INTO user SET facebook_user_id = :fb_uid, token_id = :token_id, facebook_tooken_id = :fb_token_id '), {'fb_uid': profile['id'], 'token_id': guid_val, 'fb_token_id': fb_token})
                return guid_val
    else:
        return "'fb_api_key' is empty"


def guid(salt, fb_user_id):
    """
    Generates a universally unique ID.
    Any arguments only create more randomness.
    """
    t = long( time.time() * 1000 )
    r = long( random.random()*100000000000000000L )
    try:
        a = socket.gethostbyname( socket.gethostname() )
    except:
        # if we can't get a network address, just imagine one
        a = random.random()*100000000000000000L
    data = str(t)+' '+str(r)+' '+str(a)+' '+str(salt)
    data = hashlib.md5(data).hexdigest()+hashlib.md5((str(fb_user_id)+salt)).hexdigest()+uuid.uuid4().hex

    return data


if __name__ == "__main__":
    app.run(debug=True)
